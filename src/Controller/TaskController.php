<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use App\Repository\ToDoListRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/task')]
class TaskController extends AbstractController
{
    #[Route('{listId}/new', name: 'app_task_new')]
    public function new(Request $request, EntityManagerInterface $entityManager,
        int $listId, ToDoListRepository $listRepository
    ): Response
    {

        $list = $listRepository->findOneBy(["id" => $listId]);
        $task = new Task();
        $task->setToDoList($list);
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('app_lists_show', [
                'id' => $listId
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('task/new.html.twig', [
            'task' => $task,
            'form' => $form,
        ]);
    }

    #[Route('{listId}/edit/{id}', name: 'app_task_edit')]
    public function edit(Request $request, Task $task,
                         EntityManagerInterface $entityManager,
                        int $listId): Response
    {
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_lists_show', [
                'id' => $listId
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('task/edit.html.twig', [
            'task' => $task,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_task_delete')]
    public function delete(Request $request, Task $task, EntityManagerInterface $entityManager): Response
    {
            $entityManager->remove($task);
            $entityManager->flush();
        return $this->redirectToRoute('app_main_index', [], Response::HTTP_SEE_OTHER);
    }
}
