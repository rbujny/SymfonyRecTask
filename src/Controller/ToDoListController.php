<?php

namespace App\Controller;

use App\Entity\ToDoList;
use App\Form\ToDoListType;
use App\Repository\ToDoListRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/lists')]
class ToDoListController extends AbstractController
{
    #[Route('/', name: 'app_lists')]
    public function index(ToDoListRepository $listRepository): Response
    {
        $this->denyAccessUnlessGranted("IS_AUTHENTICATED_FULLY");
        return $this->render('list/index.html.twig', [
            "lists" => $listRepository->findBy(["User" => $this->getUser()])
        ]);
    }

    #[Route('/new', name: 'app_lists_new')]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $toDoList = new ToDoList();
        $toDoList->setUser($this->getUser());
        $form = $this->createForm(ToDoListType::class, $toDoList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($toDoList);
            $entityManager->flush();

            return $this->redirectToRoute('app_lists', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('list/new.html.twig', [
            'list' => $toDoList,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/show', name: 'app_lists_show')]
    public function show(ToDoList $toDoList): Response
    {
        return $this->render('list/show.html.twig', [
            'list' => $toDoList,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_lists_edit')]
    public function edit(Request $request, ToDoList $toDoList, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ToDoListType::class, $toDoList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_lists', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('list/edit.html.twig', [
            'list' => $toDoList,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_lists_delete')]
    public function delete(ToDoList $list, EntityManagerInterface $entityManager): Response
    {
            $entityManager->remove($list);
            $entityManager->flush();

        return $this->redirectToRoute('app_lists', [], Response::HTTP_SEE_OTHER);
    }
}
