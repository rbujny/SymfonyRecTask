<?php

namespace App\Form;

use App\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', \Symfony\Component\Form\Extension\Core\Type\TextType::class,
                [
                    'attr' => ['class' => "form-control"]
                ])
            ->add('Description', TextareaType::class,
            [
                'attr' => ['class' => "form-control"]
            ])
            ->add('Priority', ChoiceType::class,
            [
                'choices'  => [
                    'Low' => 'Low',
                    'Medium'=> 'Medium',
                    'High' => 'High',
                ],
                'attr' => ['class' => "form-control"]
            ])
            ->add('CompletionDate', DateType::class,
            [
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker form-control',
                    ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}
