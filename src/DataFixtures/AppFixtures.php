<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\ToDoList;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail("admin@admin.com");
        $user->setPassword($this->hasher->hashPassword($user,"admin"));
        $manager->persist($user);

        $list = new ToDoList();
        $list->setName("First list");
        $list->setUser($user);
        $manager->persist($list);
        $list2 = new ToDoList();
        $list2->setName("Second list");
        $list2->setUser($user);
        $manager->persist($list2);
        $list3 = new ToDoList();
        $list3->setName("Third list");
        $list3->setUser($user);
        $manager->persist($list3);
        for($i=1; $i<=10; $i++)
        {
            $task = new Task();
            $task->setName("Task ".$i);
            $task->setDescription("Some task description");
            $task->setPriority("High");
            $task->setCompletionDate(new \DateTime());
            $task->setToDoList($list);
            $manager->persist($task);
        }
        for($j=1; $j<=3; $j++)
        {
            $task = new Task();
            $task->setName("Task ".$j);
            $task->setDescription("Some task description");
            $task->setPriority("High");
            $task->setCompletionDate(new \DateTime());
            $task->setToDoList($list2);
            $manager->persist($task);
        }
        $manager->flush();
    }
}
