import './styles/app.css';
import $ from 'jquery';
import 'bootstrap';

document.addEventListener('DOMContentLoaded', function() {
    // Get the date input element
    let dateInput = document.getElementById('task_CompletionDate');

    // Get today's date in the format "YYYY-MM-DD"
    let today = new Date().toISOString().split('T')[0];

    // Set the minimum attribute of the date input to today's date
    dateInput.min = today;
});

$(document).ready(function() {
    $(".delete-checkbox").change(function() {
        if ($(this).is(":checked")) {
            const taskId = $(this).data("task-id");
            const $row = $(`.delete-checkbox[data-task-id="${taskId}"]`).closest('tr')
            $.ajax({
                url: `/task/${taskId}`,
                type: 'POST',
                success: function(response) {
                    $row.css("text-decoration", "line-through");
                    setTimeout(function() {
                        $row.remove();
                    }, 500)
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }
    });
});