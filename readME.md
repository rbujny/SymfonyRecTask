# SymfonyRecTask [PL] 🔍

## 📖 O zadaniu

* W aplikacji mogę się zarejestrować oraz zalogować.
* Do obsługi maila należy wykorzystać wbudowany komponent Symfony, który służy do obsługi zadań w tle.
* Jako zalogowany użytkownik mogę stworzyć wiele list TODO i każda z tych list może mieć wiele pozycji.
* Po zaznaczeniu checkboxem, że dana pozycja jest zakończona to cały ten element wykreślamy.
* Zapis tego działania ma odbywać się asynchronicznie
* Na części frontowej proszę wykorzystać bootstrap oraz Jquery.

## 🛠 Technologie użyte do realizacji zadania
**🍊 Frontend**

- HTML/Twig 🌱

- CSS/Bootstrap 🔵🟣

- JS/JQuery 🟡

**🍑 Backend**

- Symfony 5.4 (LTS) 🐘

- MySQL 🐬

- Docker 🐳


## ⚙️ Konfiguracja lokalna
Po sklonowaniu repozytorium należy zainstalować composer:

```
composer install
```
Następnie zbudować kontener wraz z bazą w dockerze poprzez komende:

```
docker compose up -d
```

Oraz uruchomić yarn'a poprzez:

```
yarn watch
```

## 📙 Konfiguracja bazy danych

Do projektu została użyta baza MySQL znajdująca się we wcześniej wspomnianym dockerze.
Aby zbudować bazę danych, dokonać potrzebnych migracji i załadować przykładowe dane:
```
    symfony console doctrine:database:create
    symfony console doctrine:migrations:migrate
    symfony console doctrine:fixtures:load
```

## 🖥️ Start serwera

Aby uruchomić serwer należy wpisać:

```
    symfony serve -d
```

## ⚙️ Konfiguracja na stronie

Aby korzystać ze strony, można zalogować się za pomocą przykładowego konta:
- email: `admin@admin.com` z hasłem`admin`
- lub przejść pod adres `/register` and stworzyć własnego użytkownika!

❗ **Pamiętaj** tylko zarejestrowany użytkownik może korzystać z funkcjonalności strony!

### ✍️ Autor

*Radosław Bujny*
